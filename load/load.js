import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
  stages: [
    { duration: '30s', target: 5 },
    { duration: '30s', target: 10 },
    { duration: '30s', target: 40 },
    { duration: '20s', target: 5 },
  ],
};

export default function () {
let n = Math.floor(Math.random() * 100000)
const payload = JSON.stringify({
    login: 'user' + n,
    password: 'super strong',
    username: 'Test'
  });

  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const res = http.post('http://ts.bar.com/register', payload, params);
  const token = res.body

  const headers = {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
  };

  const addOrganization = `
   mutation addOrganization($name: String!) {
       addOrganization(name: $name) {
           id
           name
       }
   }`;

  const addProject = `
   mutation addProject($orgId: ID!, $name: String!) {
       addProject(organizationId: $orgId, name: $name) {
           id
           name
       }
   }`;

  const addTask = `
  mutation addTask($title: String!, $projectId: ID!, $date: Date!, $duration: Int!) {
      addTask(title: $title, projectId: $projectId, date: $date, duration: $duration) {
          id
      }
  }
  `

  const res2 = http.post('http://ts.bar.com/graphql', JSON.stringify({query: addOrganization, variables: {name: "org" + n}}), headers);
  var orgId = JSON.parse(res2.body).data.addOrganization.id
  console.log(orgId)
  const res3 = http.post('http://ts.bar.com/graphql', JSON.stringify({query: addProject, variables: {orgId: orgId, name: "proj"+n}}), headers);
  var projId = JSON.parse(res3.body).data.addProject.id
  console.log(projId)
  for (let i = 0; i < 100; i++) {
    http.post('http://ts.bar.com/graphql', JSON.stringify({query: addTask, variables: {title: "Test title " + i, projectId: projId, date: "0001-01-01", duration: i}}), headers);
  }
}

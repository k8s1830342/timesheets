package work.moscichowski.timesheets.fixtures

import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.graphql.test.tester.HttpGraphQlTester
import org.springframework.http.HttpHeaders
import work.moscichowski.timesheets.controller.RegisterRequest
import work.moscichowski.timesheets.repository.OrganizationWithRole

class Fixtures {
    companion object {
        fun `one org one proj two users`(restTemplate: TestRestTemplate, graphQlTester: HttpGraphQlTester, port: Int): Pair<HttpGraphQlTester, HttpGraphQlTester> {
            val ownerToken = restTemplate.postForEntity("http://localhost:$port/register", RegisterRequest("someone", "somepassword", "Test User"), String::class.java).body
            val ownerTester = graphQlTester.mutate().headers { headers: HttpHeaders ->
                headers.set("Authorization", "Bearer $ownerToken") }.build()

            val invitedToken = restTemplate.postForEntity("http://localhost:$port/register", RegisterRequest("invited", "somepassword", "Invited User"), String::class.java).body
            val invitedTester = graphQlTester.mutate().headers { headers: HttpHeaders ->
                headers.set("Authorization", "Bearer $invitedToken") }.build()

            val orgId = ownerTester
                .documentName("addOrganization")
                .variable("name", "Test Org")
                .execute()
                .path("addOrganization.id").entity(String::class.java).get().toLong()

            val invitationId = ownerTester
                .documentName("inviteToOrganization")
                .variable("userId", 2)
                .variable("organizationId", orgId)
                .variable("role", "Member")
                .execute()
                .path("inviteUser").entity(String::class.java).get().toLong()

            invitedTester
                .documentName("acceptInviteToOrganization")
                .variable("invitationId", invitationId)
                .execute()
                .path("acceptInvitation").entity(String::class.java).get()

            val organizations = invitedTester
                .documentName("getOrganizations")
                .execute()
                .path("getOrganizations").entityList(OrganizationWithRole::class.java).get()

            val expected = listOf(
                OrganizationWithRole(1, "Test Org", "Member")
            )

            val projId = ownerTester
                .documentName("addProject")
                .variable("orgId", orgId)
                .variable("name", "Test Project")
                .execute()
                .path("addProject.id")
                .entity(String::class.java).get().toLong()

            ownerTester
                .documentName("addUserToProject")
                .variable("projectId", projId)
                .variable("userId", 2)
                .variable("role", "Test Role")
                .execute()
                .path("addUserToProject").entity(String::class.java).get()

            return ownerTester to invitedTester
        }
    }
}
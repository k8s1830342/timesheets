package work.moscichowski.timesheets.controller

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureGraphQlTester
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.graphql.test.tester.HttpGraphQlTester
import org.springframework.http.HttpHeaders
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import work.moscichowski.timesheets.fixtures.Fixtures
import work.moscichowski.timesheets.repository.*
import work.moscichowski.timesheets.service.Task
import work.moscichowski.timesheets.service.TestConf
import work.moscichowski.timesheets.storage.OrganizationEntity
import work.moscichowski.timesheets.storage.ProjectEntity
import work.moscichowski.timesheets.storage.UserEntity
import java.time.LocalDate

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [TestConf::class])
@AutoConfigureGraphQlTester
class FullTest {
    @LocalServerPort
    private var port: Int = 0

    @Autowired
    private lateinit var graphQlTester: HttpGraphQlTester

    @Autowired
    private lateinit var taskRepository: TaskRepository

    @Autowired
    private lateinit var projectRepository: ProjectRepository

    @Autowired
    private lateinit var organizationRepository: OrganizationRepository

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var restTemplate: TestRestTemplate

    @Autowired
    private lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

    @Test
    fun `add task`() {
        val org = OrganizationEntity()
        val user = UserEntity("Test")
        user.login = "user"
        user.password = bCryptPasswordEncoder.encode("password")
        val proj = ProjectEntity("Test proj")

        userRepository.save(user)
        organizationRepository.save(org)
        proj.organization = org
//        proj.users = setOf(user)
        projectRepository.save(proj)

        val token = restTemplate.postForEntity(
            "http://localhost:$port/login",
            LoginRequest("user", "password"),
            String::class.java
        ).body

        val tester = graphQlTester.mutate().headers { headers: HttpHeaders ->
            headers.set("Authorization", "Bearer $token")
        }.build()

        val newTaskId = tester
            .documentName("addTask")
            .variable("title", "Test Task")
            .variable("projectId", 1)
            .variable("date", "0001-02-03")
            .variable("duration", 321)
            .execute()
            .path("addTask.id").entity(String::class.java).get().toLong()

        tester
            .documentName("myTasks")
            .variable("from", "0001-02-01")
            .variable("to", "0001-02-05")
            .variable("orgId", "1")
            .execute()
            .path("tasks.tasks[0]")
            .matchesJson("{\"id\":\"$newTaskId\",\"title\":\"Test Task\"}")

        tester
            .documentName("updateTask")
            .variable("taskID", newTaskId)
            .variable("title", "New task name")
            .execute()
            .path("updateTask.id")
            .matchesJson("\"4567\"")
    }

    @Test
    fun `add task as manager`() {
        val org = OrganizationEntity()
        val manager = UserEntity("Mr. Manager")
        val user = UserEntity("User")
        manager.login = "manager"
        manager.password = bCryptPasswordEncoder.encode("password")
        val proj = ProjectEntity("Test proj")
//        proj.users = setOf(user)
        userRepository.save(manager)
        userRepository.save(user)
        organizationRepository.save(org)
        proj.organization = org
        proj.managers = setOf(manager)
        projectRepository.save(proj)


        val token = restTemplate.postForEntity(
            "http://localhost:$port/login",
            LoginRequest("manager", "password"),
            String::class.java
        ).body

        val tester = graphQlTester.mutate().headers { headers: HttpHeaders ->
            headers.set("Authorization", "Bearer $token")
        }.build()

        val newTaskId = tester
            .documentName("addTaskAsManager")
            .variable("userId", user.id)
            .variable("title", "Test Task")
            .variable("projectId", proj.id)
            .variable("date", "0001-02-03")
            .variable("duration", 321)
            .execute()
            .path("addTask.id").entity(String::class.java).get().toLong()
        println(newTaskId)
    }

    @Test
    fun `full reg`() {
        val token = restTemplate.postForEntity(
            "http://localhost:$port/register",
            RegisterRequest("someone", "somepassword", "Test User"),
            String::class.java
        ).body
        val tester = graphQlTester.mutate().headers { headers: HttpHeaders ->
            headers.set("Authorization", "Bearer $token")
        }.build()

        val orgId = tester
            .documentName("addOrganization")
            .variable("name", "Test Org")
            .execute()
            .path("addOrganization.id").entity(String::class.java).get().toLong()

        val projId = tester
            .documentName("addProject")
            .variable("orgId", orgId)
            .variable("name", "Test Proj")
            .execute()
            .path("addProject.id").entity(String::class.java).get().toLong()

        val newTaskId = tester
            .documentName("addTask")
            .variable("title", "Test Task")
            .variable("projectId", projId)
            .variable("date", "0001-02-03")
            .variable("duration", 321)
            .execute()
            .path("addTask.id").entity(String::class.java).get().toLong()

        tester
            .documentName("myTasks")
            .variable("from", "0001-02-01")
            .variable("to", "0001-02-05")
            .variable("orgId", orgId)
            .execute()
            .path("tasks.tasks[0]")
            .matchesJson("{\"id\":\"$newTaskId\",\"title\":\"Test Task\"}")
    }

    @Test
    fun `invite to organization and accept`() {
        val ownerToken = restTemplate.postForEntity(
            "http://localhost:$port/register",
            RegisterRequest("someone", "somepassword", "Test User"),
            String::class.java
        ).body
        val ownerTester = graphQlTester.mutate().headers { headers: HttpHeaders ->
            headers.set("Authorization", "Bearer $ownerToken")
        }.build()

        val invitedToken = restTemplate.postForEntity(
            "http://localhost:$port/register",
            RegisterRequest("invited", "somepassword", "Invited User"),
            String::class.java
        ).body
        val invitedTester = graphQlTester.mutate().headers { headers: HttpHeaders ->
            headers.set("Authorization", "Bearer $invitedToken")
        }.build()

        val orgId = ownerTester
            .documentName("addOrganization")
            .variable("name", "Test Org")
            .execute()
            .path("addOrganization.id").entity(String::class.java).get().toLong()

        val ownerOrganizations = ownerTester.documentName("getOrganizations")
            .execute()
            .path("getOrganizations").entityList(OrganizationWithRole::class.java).get()

        Assertions.assertEquals(
            listOf(
                OrganizationWithRole(1, "Test Org", "Owner")
            ), ownerOrganizations
        )

        val invitationId = ownerTester
            .documentName("inviteToOrganization")
            .variable("userId", 2)
            .variable("organizationId", orgId)
            .variable("role", "Member")
            .execute()
            .path("inviteUser").entity(String::class.java).get().toLong()

        invitedTester
            .documentName("acceptInviteToOrganization")
            .variable("invitationId", invitationId)
            .execute()
            .path("acceptInvitation").entity(String::class.java).get()

        val organizations = invitedTester
            .documentName("getOrganizations")
            .execute()
            .path("getOrganizations").entityList(OrganizationWithRole::class.java).get()

        val expected = listOf(
            OrganizationWithRole(1, "Test Org", "Member")
        )

        Assertions.assertEquals(expected, organizations)

        val projId = ownerTester
            .documentName("addProject")
            .variable("orgId", orgId)
            .variable("name", "Test Project")
            .execute()
            .path("addProject.id")
            .entity(String::class.java).get().toLong()

        var projects = ownerTester
            .documentName("getProjects")
            .variable("orgId", orgId)
            .execute()
            .path("getProjects").entity(Map::class.java).get()

        Assertions.assertEquals(1, ((projects as LinkedHashMap)["projects"] as List<*>).size)

        projects = invitedTester
            .documentName("getProjects")
            .variable("orgId", orgId)
            .execute()
            .path("getProjects").entity(Map::class.java).get()

        Assertions.assertEquals(0, ((projects as LinkedHashMap)["projects"] as List<*>).size)

        ownerTester
            .documentName("addUserToProject")
            .variable("projectId", projId)
            .variable("userId", 2)
            .variable("role", "Test Role")
            .execute()
            .path("addUserToProject").entity(String::class.java).get()

        val projs2 = invitedTester
            .documentName("getProjects")
            .variable("orgId", orgId)
            .execute()
            .path("getProjects").entity(Map::class.java).get()

        Assertions.assertEquals(1, ((projs2 as LinkedHashMap)["projects"] as List<*>).size)
    }

    @Test
    fun `add tasks to project`() {
        val (owner, member) = Fixtures.`one org one proj two users`(
            restTemplate = restTemplate,
            graphQlTester = graphQlTester,
            port = port
        )

        val orgId = owner.documentName("getOrganizations")
            .execute()
            .path("getOrganizations[0].id").entity(Long::class.java).get()

        val projId = owner.documentName("getProjects")
            .variable("orgId", orgId)
            .execute()
            .path("getProjects.projects[0].id").entity(Long::class.java).get()

        owner.documentName("addTask")
            .variable("title", "Owner task")
            .variable("projectId", projId)
            .variable("date", "2024-01-04")
            .variable("duration", 123)
            .execute()
            .path("addTask.id").entity(Long::class.java).get()

        val memberId = member.documentName("getCurrentUser")
            .execute()
            .path("getCurrentUser.id").entity(Long::class.java).get()

        owner.documentName("addTaskAsManager")
            .variable("userId", memberId)
            .variable("title", "Member task added by owner")
            .variable("projectId", projId)
            .variable("date", "2024-01-03")
            .variable("duration", 321)
            .execute()
            .path("addTask.id").entity(Long::class.java).get()

        val memberTasks = member.documentName("myTasks")
            .variable("from", "2024-01-01")
            .variable("to", "2024-01-05")
            .variable("orgId", orgId)
            .execute()
            .path("tasks.tasks").entityList(Map::class.java).get()

        val ownerTasks = owner.documentName("myTasks")
            .variable("from", "2024-01-01")
            .variable("to", "2024-01-05")
            .variable("orgId", orgId)
            .execute()
            .path("tasks.tasks").entityList(Map::class.java).get()

        Assertions.assertEquals(1, ownerTasks.size)
        Assertions.assertEquals(1, memberTasks.size)
    }

    @Test
    fun `get report`() {
        val (owner, member) = Fixtures.`one org one proj two users`(
            restTemplate = restTemplate,
            graphQlTester = graphQlTester,
            port = port
        )

        val orgId = owner.documentName("getOrganizations")
            .execute()
            .path("getOrganizations[0].id").entity(Long::class.java).get()

        val projId = owner.documentName("getProjects")
            .variable("orgId", orgId)
            .execute()
            .path("getProjects.projects[0].id").entity(Long::class.java).get()

        val startDate = LocalDate.of(2024, 1, 1)
        for (i in 0..100) {
            member.documentName("addTask")
                .variable("title", "Member task $i")
                .variable("projectId", projId)
                .variable("date", startDate.plusDays(i.toLong()).toString())
                .variable("duration", i+1)
                .execute()
                .path("addTask.id")
        }
        val res = member.documentName("getUserReport")
            .variable("from", "2024-01-01")
            .variable("to", "2024-01-31")
            .execute()
            .path("getUserReport.minutes").entity(Long::class.java).get()

        Assertions.assertEquals(496, res)
    }

    @Test
    fun `just testing`() {
        val token = restTemplate.postForEntity(
            "http://localhost:$port/register",
            RegisterRequest("someone", "somepassword", "Test User"),
            String::class.java
        ).body

        val tester = graphQlTester.mutate().headers { headers: HttpHeaders ->
            headers.set("Authorization", "Bearer $token")
        }.build()

        val orgId = tester
            .documentName("parent")
            .execute()
            .path("parents").entityList(Map::class.java).get()
        println()
    }
}

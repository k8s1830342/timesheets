package work.moscichowski.timesheets.controller

import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureGraphQlTester
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.graphql.test.tester.GraphQlTester
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.context.support.WithUserDetails
import work.moscichowski.timesheets.repository.UserRepository
import work.moscichowski.timesheets.service.*
import java.time.LocalDate

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [TestConf::class])
@AutoConfigureGraphQlTester
class ControllerTests {
    @Autowired
    private lateinit var graphQlTester: GraphQlTester

    @MockBean
    lateinit var service: TaskService

    @Test
    @WithUserDetails(value = "manager", userDetailsServiceBeanName = "myUserDetailsService")
    fun dontknowyet2() {
        val tasks = listOf(
            Task(1, "Test task", LocalDate.of(1,1,3), 123, User(1, ""), Project(1, "")),
            Task(1, "Test task 2", LocalDate.of(1,1,5), 321, User(1, ""), Project(1, "")),
        )
        val paginated = PaginatedTasks(2, tasks, 0)

        `when`(service.getTasks(null,  setOf(1), setOf(1), LocalDate.of(1,1,3), LocalDate.of(1,1,4)))
            .thenReturn(paginated)
        graphQlTester
            .documentName("report")
            .execute()
            .path("tasks")
            .matchesJson("{\"tasks\":[{\"title\":\"Test task\",\"date\":\"0001-01-03\"},{\"title\":\"Test task 2\",\"date\":\"0001-01-05\"}]}")

    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `my tasks in org`() {
        val tasks = listOf(
            Task(1, "Test task", LocalDate.of(1,1,3), 123, User(1, ""), Project(1, "")),
            Task(1, "Test task 2", LocalDate.of(1,1,5), 321, User(1, ""), Project(1, "")),
        )
        val paginated = PaginatedTasks(2, tasks, 0)

        `when`(service.getMyTasks(321, LocalDate.of(1,1,1), LocalDate.of(1,1,5)))
            .thenReturn(paginated)
        graphQlTester
            .documentName("myTasks")
            .variable("from", "0001-01-01")
            .variable("to", "0001-01-05")
            .variable("orgId", 321)
            .execute()
            .path("tasks")
            .matchesJson("{\"tasks\":[{\"title\":\"Test task\",\"date\":\"0001-01-03\"},{\"title\":\"Test task 2\",\"date\":\"0001-01-05\"}]}")

    }
}

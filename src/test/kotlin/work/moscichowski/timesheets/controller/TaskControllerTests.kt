package work.moscichowski.timesheets.controller

import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureGraphQlTester
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.graphql.test.tester.GraphQlTester
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.security.test.context.support.WithUserDetails
import work.moscichowski.timesheets.exceptions.ProjectDontExist
import work.moscichowski.timesheets.repository.UserRepository
import work.moscichowski.timesheets.service.*
import java.time.LocalDate
import java.util.function.Predicate

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [TestConf::class])
@AutoConfigureGraphQlTester
class TaskControllerTests {
    @Autowired
    private lateinit var graphQlTester: GraphQlTester

    @MockBean
    lateinit var service: TaskService

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `proper ProjectDontExist handling`() {

        `when`(service.getTasks(null, setOf(1), setOf(1), LocalDate.of(1,1,3), LocalDate.of(1,1,4)))
            .thenThrow(ProjectDontExist())
        graphQlTester
            .documentName("report")
            .execute().errors().expect {
                it.message == "Project not exists"
            }.verify()
    }
}

package work.moscichowski.timesheets.controller

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureGraphQlTester
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.test.tester.GraphQlTester
import org.springframework.security.test.context.support.WithUserDetails
import org.springframework.stereotype.Controller
import work.moscichowski.timesheets.exceptions.ProjectDontExist
import work.moscichowski.timesheets.service.Organization
import work.moscichowski.timesheets.service.OrganizationService
import work.moscichowski.timesheets.service.TaskService
import work.moscichowski.timesheets.service.TestConf
import java.time.LocalDate

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [TestConf::class])
@AutoConfigureGraphQlTester
class OrganizationControllerTests {
    @Autowired
    private lateinit var graphQlTester: GraphQlTester

    @MockBean
    lateinit var service: OrganizationService

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `add organization`() {

        `when`(service.addOrganization("Test")).thenReturn(Organization(123, "Test"))
        graphQlTester
            .documentName("addOrganization")
            .variable("name", "Test")
            .execute()
            .path("addOrganization")
            .matchesJson("{\"id\":\"123\",\"name\":\"Test\"}")
    }

//    @Test
//    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
//    fun `get user organizations`() {
//
//        `when`(service.getUserOrganizations()).thenReturn(listOf(Organization(123, "Test"), Organization(321, "Next")))
//        graphQlTester
//            .documentName("getOrganizations")
//            .execute()
//            .path("getOrganizations")
//            .matchesJson("[{\"id\":\"123\",\"name\":\"Test\"},{\"id\":\"321\",\"name\":\"Next\"}]")
//    }
}

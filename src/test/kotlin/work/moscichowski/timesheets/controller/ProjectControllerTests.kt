package work.moscichowski.timesheets.controller

import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.graphql.tester.AutoConfigureGraphQlTester
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.test.tester.GraphQlTester
import org.springframework.security.test.context.support.WithUserDetails
import org.springframework.stereotype.Controller
import work.moscichowski.timesheets.exceptions.ProjectDontExist
import work.moscichowski.timesheets.service.*
import java.time.LocalDate

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [TestConf::class])
@AutoConfigureGraphQlTester
class ProjectControllerTests {
    @Autowired
    private lateinit var graphQlTester: GraphQlTester

    @MockBean
    lateinit var service: ProjectService

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `add project`() {

        `when`(service.addProject(123, "Test")).thenReturn(Project(321, "Test"))
        graphQlTester
            .documentName("addProject")
            .variable("orgId", 123)
            .variable("name", "Test")
            .execute()
            .path("addProject")
            .matchesJson("{\"id\":\"321\",\"name\":\"Test\"}")
    }

//    @Test
//    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
//    fun `get projects`() {
//
//        `when`(service.getProjects(123)).thenReturn(PaginatedProjects(2, listOf(Project(321, "Test")), 3))
//        graphQlTester
//            .documentName("getProjects")
//            .variable("orgId", 123)
//            .execute()
//            .path("getProjects")
//            .matchesJson("{\"projects\":[{\"id\":\"321\",\"name\":\"Test\"}],\"total\":2,\"pageNo\":3}")
//    }
}

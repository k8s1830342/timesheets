package work.moscichowski.timesheets.controller

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import work.moscichowski.timesheets.service.LoginService
import work.moscichowski.timesheets.service.TestConf


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [TestConf::class])
class LoginControllerTests {

    @LocalServerPort
    private var port: Int = 0

    @Autowired
    private lateinit var loginController: LoginController

    @Autowired
    private lateinit var restTemplate: TestRestTemplate

    @MockBean
    lateinit var service: LoginService

    @Test
    fun testLogin() {
    }

    @Test
    fun testRegister() {
        `when`(service.register("login", "password", "Test username")).thenReturn("testToken")
        val token = restTemplate.postForEntity("http://localhost:$port/register", RegisterRequest("login", "password", "Test username"), String::class.java).body

        assertEquals("testToken", token)
    }

}
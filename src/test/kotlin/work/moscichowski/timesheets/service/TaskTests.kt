package work.moscichowski.timesheets.service

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import work.moscichowski.timesheets.repository.TaskRepository
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.security.test.context.support.WithUserDetails
import work.moscichowski.timesheets.exceptions.NoManagerPermissions
import work.moscichowski.timesheets.exceptions.UserNotInProject
import work.moscichowski.timesheets.repository.ProjectRepository
import work.moscichowski.timesheets.repository.UserRepository
import work.moscichowski.timesheets.storage.ProjectEntity
import work.moscichowski.timesheets.storage.TaskEntity
import work.moscichowski.timesheets.storage.UserEntity
import java.time.LocalDate
import java.util.*

@SpringBootTest(classes = [TestConf::class])
class TaskTests {


    @Autowired
    lateinit var taskService: TaskService

    @MockBean
    lateinit var taskRepository: TaskRepository

    @MockBean
    lateinit var projectRepository: ProjectRepository

    @MockBean
    lateinit var userRepository: UserRepository

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun simpleAddTask() {
        val testProject = ProjectEntity("Test")
        testProject.id = 321
        val testUser = UserEntity("Test")
        testUser.id = 1
//        testProject.users = setOf(testUser)
        val testDate = LocalDate.of(1, 2, 3)
        val testMinutes = 123
        val insertedTask = TaskEntity("Test title", testUser, testProject, testDate, testMinutes)
        val returnedTask = TaskEntity("Test title", testUser, testProject, testDate, testMinutes)
        returnedTask.id = 12345

        `when`(projectRepository.findById(1)).thenReturn(Optional.of(testProject))
        `when`(userRepository.findById(1)).thenReturn(Optional.of(testUser))
        `when`(taskRepository.save(insertedTask)).thenReturn(returnedTask)

        val addedTask = taskService.addTask("Test title", 1, testDate, testMinutes)

        assertEquals("Test title", addedTask.title)
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `user cant add tasks to project that he is not a part of`() {
        val projectId: Long = 6546
        val testProject = ProjectEntity("Test")
        testProject.id = projectId
        val testUser = UserEntity()
        testUser.id = 42
//        testProject.users = setOf(testUser)
        val testDate = LocalDate.of(1, 2, 3)
        val testMinutes = 123
        `when`(projectRepository.findById(projectId)).thenReturn(Optional.of(testProject))
        `when`(userRepository.findById(1)).thenReturn(Optional.of(testUser))

        assertThrows(UserNotInProject::class.java) {
            taskService.addTask("Test title", projectId, testDate, testMinutes)
        }
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `manager can add tasks for other users`() {
        val projectId: Long = 1
        val testProject = ProjectEntity("Test")
        testProject.id = projectId
        val testUser = UserEntity("test name")
        testUser.id = 42
//        testProject.users = setOf(testUser)
        val manager = UserEntity("Mr. Manager")
        manager.id = 1
//        manager.projectsAsManager = mutableSetOf(testProject)
//        testProject.users = setOf(testUser)
        val testDate = LocalDate.of(1, 2, 3)
        val testMinutes = 123
        `when`(projectRepository.findById(projectId)).thenReturn(Optional.of(testProject))
        `when`(userRepository.findById(testUser.id!!)).thenReturn(Optional.of(testUser))
        `when`(userRepository.findById(manager.id!!)).thenReturn(Optional.of(manager))
        val taskEntity = TaskEntity("Test title", testUser, testProject, testDate, testMinutes)
        val returnEntity = TaskEntity("Test title", testUser, testProject, testDate, testMinutes)
        returnEntity.id = 123
        `when`(taskRepository.save(taskEntity)).thenReturn(returnEntity)

        val task = taskService.addTask(testUser.id!!, "Test title", projectId, testDate, testMinutes)
        assertEquals(123, task.id)
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `manager can't add tasks for other users when user is not in project`() {
        val projectId: Long = 1
        val testProject = ProjectEntity("Test")
        testProject.id = projectId
        val testUser = UserEntity("test name")
        testUser.id = 42
        val manager = UserEntity("Mr. Manager")
        manager.id = 1
//        manager.projectsAsManager = mutableSetOf(testProject)
        val testDate = LocalDate.of(1, 2, 3)
        val testMinutes = 123
        `when`(projectRepository.findById(projectId)).thenReturn(Optional.of(testProject))
        `when`(userRepository.findById(testUser.id!!)).thenReturn(Optional.of(testUser))
        `when`(userRepository.findById(manager.id!!)).thenReturn(Optional.of(manager))

        assertThrows(UserNotInProject::class.java) {
            taskService.addTask(testUser.id!!, "Test title", projectId, testDate, testMinutes)
        }
    }

    @Test
    @WithUserDetails(value = "user", userDetailsServiceBeanName = "myUserDetailsService")
    fun `user can't add tasks for other users`() {
        val projectId: Long = 1
        val testProject = ProjectEntity("Test")
        testProject.id = projectId
        val testUser = UserEntity()
        testUser.id = 42
//        testProject.users = setOf(testUser)
        val testDate = LocalDate.of(1, 2, 3)
        val testMinutes = 123
        val manager = UserEntity("mr Manager")
        val testProject2 = ProjectEntity("Test2")
        testProject2.id = 2

//        manager.projectsAsManager = mutableSetOf(testProject2)
        `when`(userRepository.findById(1)).thenReturn(Optional.of(manager))

        `when`(projectRepository.findById(projectId)).thenReturn(Optional.of(testProject))
        `when`(userRepository.findById(testUser.id!!)).thenReturn(Optional.of(testUser))

        assertThrows(NoManagerPermissions::class.java) {
            taskService.addTask(testUser.id!!, "Test title", projectId, testDate, testMinutes)
        }
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `user gets his tasks for date range in organization`() {
        val from = LocalDate.of(1, 1, 1)
        val to = LocalDate.of(1, 1, 5)
        val projectId: Long = 1
        val testProject = ProjectEntity("Test")
        testProject.id = projectId
        val user = UserEntity("Test1")
        user.id = 32
        val testTask = TaskEntity("Test task", user, testProject, LocalDate.of(1, 1, 3), 123)
        testTask.id = 123
        `when`(taskRepository.getTasks(null, setOf(), setOf(1), from, to, Pageable.ofSize(100))).thenReturn(
            PageImpl(
                listOf(
                    testTask
                )
            )
        )
//        val tasks = taskService.getMyTasks(321, from = from, to = to)

//        assertEquals(
//            PaginatedTasks(
//                1,
//                listOf(
//                    Task(
//                        testTask.id!!,
//                        "Test task",
//                        LocalDate.of(1, 1, 3),
//                        123,
//                        User(32, "Test1"),
//                        Project(1, "Test")
//                    )
//                ),
//                0
//            ), tasks
//        )
    }

    @Test
    @WithUserDetails(value = "user", userDetailsServiceBeanName = "myUserDetailsService")
    fun `user can edit his tasks`() {

        val user = UserEntity()
        user.id = 1
        val project = ProjectEntity()

        `when`(taskRepository.findById(456)).thenReturn(Optional.of(TaskEntity("Test task", user, project, LocalDate.of(2, 2, 3), 1)))

        taskService.updateTask(456, "New", null, null, null)
        verify(taskRepository).save(TaskEntity("New", user, project, LocalDate.of(2, 2, 3), 1))

        reset(taskRepository)
        `when`(taskRepository.findById(456)).thenReturn(Optional.of(TaskEntity("Test task", user, project, LocalDate.of(2, 2, 3), 1)))

        taskService.updateTask(456, null, null, LocalDate.of(3, 2, 3), 121)
        verify(taskRepository).save(TaskEntity("Test task", user, project, LocalDate.of(3, 2, 3), 121))
    }

    @Test
    @WithUserDetails(value = "user", userDetailsServiceBeanName = "myUserDetailsService")
    fun `user can't edit others tasks`() {
        val user = UserEntity()
        user.id = 123465
        val project = ProjectEntity()
        project.id = 1
        val testTask = TaskEntity("Test task", user, project, LocalDate.of(2, 2, 3), 1)
        val manager = UserEntity("mr Manager")
        val testProject2 = ProjectEntity("Test2")
        testProject2.id = 2

//        manager.projectsAsManager = mutableSetOf(testProject2)

        `when`(userRepository.findById(1)).thenReturn(Optional.of(manager))
        `when`(taskRepository.findById(456)).thenReturn(Optional.of(testTask))

        assertThrows(NoManagerPermissions::class.java) {
            taskService.updateTask(456, null, null, null, null)
        }
    }

    @Test
    @WithUserDetails(value = "user", userDetailsServiceBeanName = "myUserDetailsService")
    fun `user can move task to different project`() {
        val user = UserEntity()
        user.id = 1
        val project = ProjectEntity()
        project.id = 1
        val testTask = TaskEntity("Test task", user, project, LocalDate.of(2, 2, 3), 1)
        val testProject2 = ProjectEntity("Test2")
        testProject2.id = 2
//        testProject2.users = setOf(user)

        `when`(userRepository.findById(1)).thenReturn(Optional.of(user))
        `when`(taskRepository.findById(456)).thenReturn(Optional.of(testTask))
        `when`(projectRepository.findById(2)).thenReturn(Optional.of(testProject2))
        taskService.updateTask(456, null, testProject2.id, null, null)

        verify(taskRepository).save(TaskEntity("Test task", user, testProject2, LocalDate.of(2, 2, 3), 1))
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `user can't move task to project he doesn't belong to`() {
        val user = UserEntity()
        user.id = 1
        val project = ProjectEntity()
        project.id = 1
        val testTask = TaskEntity("Test task", user, project, LocalDate.of(2, 2, 3), 1)
        val testProject2 = ProjectEntity("Test2")
        testProject2.id = 2

        `when`(userRepository.findById(1)).thenReturn(Optional.of(user))
        `when`(taskRepository.findById(456)).thenReturn(Optional.of(testTask))
        `when`(projectRepository.findById(2)).thenReturn(Optional.of(testProject2))
        assertThrows(UserNotInProject::class.java) {
            taskService.updateTask(456, null, testProject2.id, null, null)
        }

    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `manager can edit others tasks in project`() {
        val user = UserEntity()
        user.id = 11234
        val project = ProjectEntity()
        project.id = 1
        val manager = UserEntity("mr Manager")
        val testProject2 = ProjectEntity("Test2")
        testProject2.id = 2

//        manager.projectsAsManager = mutableSetOf(project, testProject2)
        `when`(userRepository.findById(1)).thenReturn(Optional.of(manager))

        val testTask = TaskEntity("Test task", user, project, LocalDate.of(2, 2, 3), 1)

        `when`(taskRepository.findById(456)).thenReturn(Optional.of(testTask))

        taskService.updateTask(456, "New", null, null, null)
        verify(taskRepository).save(TaskEntity("New", user, project, LocalDate.of(2, 2, 3), 1))
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `manager can't edit others tasks in project he doesn't own`() {
        val project = ProjectEntity()
        project.id = 1123

        val manager = UserEntity("mr Manager")
        val testProject2 = ProjectEntity("Test2")
        testProject2.id = 2

        `when`(userRepository.findById(1)).thenReturn(Optional.of(manager))

        val user = UserEntity()
        user.id = 11234

        val testTask = TaskEntity("Test task", user, project, LocalDate.of(2, 2, 3), 1)

        `when`(taskRepository.findById(456)).thenReturn(Optional.of(testTask))

        assertThrows(NoManagerPermissions::class.java) {
            taskService.updateTask(456, null, null, null, null)
        }
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `manager gets tasks for date range for users in projects`() {
        val from = LocalDate.of(1, 1, 1)
        val to = LocalDate.of(1, 1, 5)

        val testProject1 = ProjectEntity("Test1")
        testProject1.id = 1
        val user1 = UserEntity("User 1")
        user1.id = 123

        val manager = UserEntity("mr Manager")
        val testProject2 = ProjectEntity("Test2")
        testProject2.id = 2

//        manager.projectsAsManager = mutableSetOf(testProject1, testProject2)
        `when`(userRepository.findById(1)).thenReturn(Optional.of(manager))

        val task1 = TaskEntity("Test task", user1, testProject1, LocalDate.of(1, 1, 3), 123)
        task1.id = 1
        val task2 = TaskEntity("Test task 2", user1, testProject1, LocalDate.of(1, 1, 5), 321)
        task2.id = 2
        `when`(
            taskRepository.getTasks(
                null,
                setOf(1, 2),
                setOf(1, 2, 3),
                from,
                to,
                Pageable.ofSize(100)
            )
        ).thenReturn(PageImpl(listOf(task1, task2)))

        val tasks = listOf(
            Task(1, "Test task", LocalDate.of(1, 1, 3), 123, User(123, "User 1"), Project(1, "Test1")),
            Task(2, "Test task 2", LocalDate.of(1, 1, 5), 321, User(123, "User 1"), Project(1, "Test1")),
        )

        val paginatedTasks = taskService.getTasks(null, setOf(1, 2, 654987), setOf(1, 2, 3), from, to)
        assertEquals(PaginatedTasks(2, tasks, 0), paginatedTasks)
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `manager gets tasks for date range for all users`() {
        val from = LocalDate.of(1, 1, 1)
        val to = LocalDate.of(1, 1, 5)

        val testProject1 = ProjectEntity("Test1")
        testProject1.id = 1
        val testProject2 = ProjectEntity("Test2")
        testProject2.id = 2
        val user1 = UserEntity("User 1")
        user1.id = 123
        val user2 = UserEntity("User 2")
        user2.id = 321

        val manager = UserEntity("Mr Manager")
        manager.id = 1
//        manager.projectsAsManager = mutableSetOf(testProject1, testProject2)

        val task1 = TaskEntity("Test task", user1, testProject1, LocalDate.of(1, 1, 3), 123)
        task1.id = 1
        val task2 = TaskEntity("Test task 2", user1, testProject2, LocalDate.of(1, 1, 5), 321)
        task2.id = 2
        val task3 = TaskEntity("Test task 3", user2, testProject1, LocalDate.of(1, 1, 5), 321)
        task3.id = 3
        val task4 = TaskEntity("Test task 4", user2, testProject2, LocalDate.of(1, 1, 5), 321)
        task4.id = 4

        val tasks = listOf(
            Task(1, "Test task", LocalDate.of(1, 1, 3), 123, User(123, "User 1"), Project(1, "Test1")),
            Task(2, "Test task 2", LocalDate.of(1, 1, 5), 321, User(123, "User 1"), Project(2, "Test2")),
            Task(3, "Test task 3", LocalDate.of(1, 1, 5), 321, User(321, "User 2"), Project(1, "Test1")),
            Task(4, "Test task 4", LocalDate.of(1, 1, 5), 321, User(321, "User 2"), Project(2, "Test2")),
        )

        `when`(userRepository.findById(1)).thenReturn(Optional.of(manager))

        `when`(
            taskRepository.getTasks(
                null,
                setOf(1, 2),
                setOf(),
                from,
                to,
                Pageable.ofSize(100)
            )
        ).thenReturn(PageImpl(listOf(task1, task2, task3, task4)))

        val paginatedTasks = taskService.getTasks(from = from, to = to)
        assertEquals(PaginatedTasks(4, tasks, 0), paginatedTasks)
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `manager gets forbidden for reporting other projects`() {
        val from = LocalDate.of(1, 1, 1)
        val to = LocalDate.of(1, 1, 5)

        val manager = UserEntity("mr Manager")
        val testProject1 = ProjectEntity("Test1")
        testProject1.id = 1
        val testProject2 = ProjectEntity("Test2")
        testProject2.id = 2

//        manager.projectsAsManager = mutableSetOf(testProject1, testProject2)
        `when`(userRepository.findById(1)).thenReturn(Optional.of(manager))

        assertThrows(NoManagerPermissions::class.java) {
            taskService.getTasks(projectIds = setOf(456), from = from, to = to)
        }
    }
}

fun UserEntity.default(): UserEntity {
    val userEntity = UserEntity("Test")
    userEntity.id = 1
    return userEntity
}
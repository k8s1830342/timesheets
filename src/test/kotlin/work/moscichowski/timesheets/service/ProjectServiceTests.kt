package work.moscichowski.timesheets.service

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.Pageable
import org.springframework.security.test.context.support.WithUserDetails
import work.moscichowski.timesheets.repository.OrganizationRepository
import work.moscichowski.timesheets.repository.ProjectRepository
import work.moscichowski.timesheets.repository.UserRepository
import work.moscichowski.timesheets.storage.OrganizationEntity
import work.moscichowski.timesheets.storage.ProjectEntity
import work.moscichowski.timesheets.storage.UserEntity
import java.util.*

@SpringBootTest(classes = [TestConf::class])
class ProjectServiceTests {

    @MockBean
    lateinit var organizationRepository: OrganizationRepository

    @MockBean
    lateinit var userRepository: UserRepository

    @MockBean
    lateinit var projectRepository: ProjectRepository

    @Autowired
    lateinit var projectService: ProjectService

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun addProject() {
        val org = OrganizationEntity()
        org.id = 123
        val user = UserEntity()
        user.id = 1
//        org.managers = mutableSetOf(user)
        `when`(userRepository.findById(1)).thenReturn(Optional.of(user))
        `when`(organizationRepository.findById(123)).thenReturn(Optional.of(org))
        val returnedEntity = ProjectEntity()
        returnedEntity.id = 3214
        returnedEntity.name = "Test project"
        `when`(projectRepository.save(argThat {
            it.name == "Test project" &&
                    it.managers == setOf(user) &&
                    it.users == setOf(user) &&
                    it.organization == org
        })).thenReturn(returnedEntity)

        val project = projectService.addProject(org.id!!, "Test project")
        assertEquals(Project(3214, "Test project"), project)
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun getProjects() {
        val org = OrganizationEntity()
        org.id = 123
        val user = UserEntity()
        user.id = 1
//        org.managers = mutableSetOf(user)
        `when`(userRepository.findById(1)).thenReturn(Optional.of(user))
        `when`(organizationRepository.findById(123)).thenReturn(Optional.of(org))
        val returnedEntity = ProjectEntity()
        returnedEntity.id = 3214
        returnedEntity.name = "Test project"
        `when`(projectRepository.getProjects(123, 1, Pageable.ofSize(100))).thenReturn(
            PageImpl(
                listOf(
                    returnedEntity
                )
            )
        )

        val projectsPage = projectService.getProjects(123)
        assertEquals(1, projectsPage.total)
        assertEquals(1, projectsPage.pageNo)
//        assertEquals(Project(3214, "Test project"), projectsPage.projects[0])
    }
}
package work.moscichowski.timesheets.service

import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.provisioning.UserDetailsManager
import work.moscichowski.timesheets.security.AuthUser


class MyUserDetailsManager(private val users: Map<String, AuthUser>): UserDetailsManager {
    override fun loadUserByUsername(username: String?): UserDetails {
        return users[username]!!
    }

    override fun createUser(user: UserDetails?) {
    }

    override fun updateUser(user: UserDetails?) {
    }

    override fun deleteUser(username: String?) {
    }

    override fun changePassword(oldPassword: String?, newPassword: String?) {
    }

    override fun userExists(username: String?): Boolean {
        return true
    }

}


@TestConfiguration
class TestConf {
    @Bean
    @Primary
    fun myUserDetailsService(): UserDetailsService {
        val users = mapOf(
            "user" to AuthUser(1),
            "manager" to AuthUser(2),
            )
        return MyUserDetailsManager(users)
    }
}
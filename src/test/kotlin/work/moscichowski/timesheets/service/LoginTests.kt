package work.moscichowski.timesheets.service

import io.jsonwebtoken.Jwts
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import work.moscichowski.timesheets.exceptions.UserAlreadyExists
import work.moscichowski.timesheets.repository.UserRepository
import work.moscichowski.timesheets.storage.UserEntity
import javax.crypto.SecretKey

@SpringBootTest(classes = [TestConf::class])
class LoginTests {

    @MockBean
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var loginService: LoginService

    @Autowired
    lateinit var key: SecretKey

    @MockBean
    lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

    @Test
    fun register() {
        val requiredUser = UserEntity()
        requiredUser.login = "user"
        requiredUser.password = "hashPassword"
        requiredUser.name = "Test username"
        val returnUser = UserEntity()
        returnUser.id = 42
        `when`(bCryptPasswordEncoder.encode("password")).thenReturn("hashPassword")
        `when`(userRepository.save(requiredUser)).thenReturn(returnUser)
        val token = loginService.register("user", "password", "Test username")

        val parseSignedClaims = Jwts.parser()
            .verifyWith(key)
            .clockSkewSeconds(3 * 60)
            .build()
            .parseSignedClaims(token)
        println(parseSignedClaims)
        assertEquals("42", parseSignedClaims.payload.subject)
    }

    @Test
    fun `cant register existing user`() {
        val existingUser = UserEntity()
        `when`(userRepository.findByLogin("user")).thenReturn(existingUser)
        Assertions.assertThrows(UserAlreadyExists::class.java) {
            loginService.register("user", "password", "Test username")
        }
    }
}
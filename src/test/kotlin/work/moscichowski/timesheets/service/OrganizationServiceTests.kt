package work.moscichowski.timesheets.service

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.security.test.context.support.WithUserDetails
import work.moscichowski.timesheets.repository.OrganizationRepository
import work.moscichowski.timesheets.repository.UserRepository
import work.moscichowski.timesheets.storage.OrganizationEntity
import work.moscichowski.timesheets.storage.UserEntity
import java.util.*

@SpringBootTest(classes = [TestConf::class])
class OrganizationServiceTests {

    @MockBean
    lateinit var organizationRepository: OrganizationRepository

    @MockBean
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var organizationService: OrganizationService

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun addOrganization() {
        val user = UserEntity()
        user.id = 1
        val retOrg = OrganizationEntity()
        retOrg.id = 1234
        retOrg.name = "Test Org"
        `when`(userRepository.findById(1)).thenReturn(Optional.of(user))
        `when`(organizationRepository.save(argThat {
            it.name == "Test Org" &&
            it.users == setOf(user)// &&
//            it.managers == setOf(user)
        })).thenReturn(retOrg)
        val org = organizationService.addOrganization("Test Org")
        assertEquals(Organization(org.id, "Test Org"), org)
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "myUserDetailsService")
    fun `get organizations`() {
        val user = UserEntity()
        user.id = 1
        val retOrg = OrganizationEntity()
        retOrg.id = 1234
        retOrg.name = "Test Org"
        `when`(userRepository.findById(1)).thenReturn(Optional.of(user))
        `when`(organizationRepository.getOrganizationsForUser(1)).thenReturn(listOf(retOrg))
        val orgs = organizationService.getUserOrganizations()
        assertEquals(listOf(Organization(1234, "Test Org")), orgs)
    }
}
package work.moscichowski.timesheets.repository

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Pageable
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import work.moscichowski.timesheets.storage.OrganizationEntity
import work.moscichowski.timesheets.storage.ProjectEntity
import work.moscichowski.timesheets.storage.TaskEntity
import work.moscichowski.timesheets.storage.UserEntity
import java.time.LocalDate
import java.util.*


@ExtendWith(SpringExtension::class)
@DataJpaTest
class TaskRepositoryTests {
    @Autowired
    lateinit var projectRepository: ProjectRepository

    @Autowired
    lateinit var organizationRepository: OrganizationRepository

    @Autowired
    lateinit var taskRepository: TaskRepository

    @Autowired
    lateinit var userRepository: UserRepository

    var org = OrganizationEntity()
    var proj = ProjectEntity("Test Proj")
    var proj2 = ProjectEntity("Second Proj")
    var proj3 = ProjectEntity("Third Proj")
    var user = UserEntity("Janusz")
    var user2 = UserEntity("Grażyna")
    var user3 = UserEntity("Hubert")

    @BeforeEach
    fun prepareDB() {
        organizationRepository.save(org)
        proj.organization = org
        proj2.organization = org
        proj3.organization = org
        projectRepository.save(proj)
        projectRepository.save(proj2)
        projectRepository.save(proj3)
//        user.organizations = setOf(org)
        userRepository.save(user)
        userRepository.save(user2)
        userRepository.save(user3)
    }

    @Test
    fun getTasksForUser() {
        taskRepository.saveAll(
            listOf(
                testTask(user),
                testTask(user2)
            )
        )
        val tasks = taskRepository.getTasks(org.id, setOf(), setOf(user.id!!), LocalDate.of(2000, 2, 1), LocalDate.of(2000, 2, 9), Pageable.ofSize(100))
        assertEquals(1, tasks.totalElements)
//        assertEquals("Test 1", tasks[0].title)
    }

    @Test
    fun getTasksForPeriod() {
        val startDate = LocalDate.of(2000, 2, 5)
        val endDate = LocalDate.of(2000, 2, 7)
        taskRepository.saveAll(
            listOf(
                TaskEntity("Test 1", user, proj, startDate, 120),
                TaskEntity("Test 2", user, proj, LocalDate.of(2000, 2, 6), 480),
                TaskEntity("Test 3", user, proj, endDate, 240),
                TaskEntity("Test 4", user, proj, LocalDate.of(2000, 2, 8), 60)
            )
        )
        val tasks = taskRepository.getTasks(null, setOf(proj.id!!), setOf(user.id!!), startDate, endDate, Pageable.ofSize(100))
        assertEquals(3, tasks.totalElements)
    }

    //
    @Test
    fun getTasksInProject() {
        taskRepository.saveAll(
            listOf(
                testTask(proj),
                testTask(proj),
                testTask(proj2)
            )
        )
        val tasks = taskRepository.getTaskInProject(proj.id!!)
        assertEquals(2, tasks.size)
        assertEquals("Test Proj", tasks[0].project?.name)
    }

    //
    @Test
    fun getTasksInProjectForPeriod() {
        val startDate = LocalDate.of(2000, 2, 5)
        val endDate = LocalDate.of(2000, 2, 7)
        taskRepository.saveAll(
            listOf(
                TaskEntity("Test 1", user, proj, startDate, 1234),
                TaskEntity("Test 1", user, proj, endDate, 1234),
                TaskEntity("Test 2", user2, proj, LocalDate.of(2000, 2, 8), 1234),
                TaskEntity("Test 3", user2, proj2, startDate, 1234)
            )
        )
        val tasks = taskRepository.getTaskInProjectForPeriod(proj.id!!, startDate, endDate)
        assertEquals(2, tasks.size)
        assertEquals("Test Proj", tasks[0].project?.name)
    }

    @Test
    fun getTasksPaginated() {
        val startDate = LocalDate.of(2000, 2, 5)
        val endDate = LocalDate.of(2000, 2, 7)
        taskRepository.saveAll(
            listOf(
                TaskEntity("Test 1", user, proj, startDate, 1234),
                TaskEntity("Test 2", user, proj, endDate, 1234),
                TaskEntity("Test 3", user2, proj, LocalDate.of(2000, 2, 8), 1234),
                TaskEntity("Test 4", user2, proj2, startDate, 1234),
                TaskEntity("Test 5", user2, proj3, startDate, 1234),
                TaskEntity("Test 6", user3, proj2, startDate, 1234)
            )
        )
        val tasks = taskRepository.getTasks(null, setOf(proj.id!!, proj2.id!!), setOf(user.id!!, user2.id!!), startDate, endDate, Pageable.ofSize(2))
        assertEquals(3, tasks.totalElements)
        assertArrayEquals(arrayOf("Test 1", "Test 2"), tasks.content.map { it.title }.toTypedArray())
        val tasksPage2 = taskRepository.getTasks(null, setOf(proj.id!!, proj2.id!!), setOf(user.id!!, user2.id!!), startDate, endDate, Pageable.ofSize(2).withPage(1))
        assertEquals("Test 4", tasksPage2.content[0].title)
    }


    fun testTask(user: UserEntity): TaskEntity =
        TaskEntity("Test 1", user, proj, LocalDate.of(2000, 2, 8), 1234)

    fun testTask(project: ProjectEntity): TaskEntity =
        TaskEntity("Test 1", user, project, LocalDate.of(2000, 2, 8), 1234)

}
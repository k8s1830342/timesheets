package work.moscichowski.timesheets.repository

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import work.moscichowski.timesheets.storage.*


@ExtendWith(SpringExtension::class)
@DataJpaTest
class UserRepositoryTests {
    @Autowired
    lateinit var projectRepository: ProjectRepository

    @Autowired
    lateinit var organizationRepository: OrganizationRepository

    @Autowired
    lateinit var taskRepository: TaskRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var userOrganisationRepository: UserOrganisationRepository

    var org = OrganizationEntity()
    var org2 = OrganizationEntity()
    var proj = ProjectEntity("Test Proj")
    var proj2 = ProjectEntity("Second Proj")
    var proj3 = ProjectEntity("Third Proj")
    var user = UserEntity("Janusz")
    var user2 = UserEntity("Grażyna")
    var user3 = UserEntity("Hubert")

    @BeforeEach
    fun prepareDB() {
        organizationRepository.save(org)
        organizationRepository.save(org2)
        proj.organization = org
        proj2.organization = org
        proj3.organization = org
        userRepository.save(user)
        userRepository.save(user2)
        userRepository.save(user3)
        proj.managers = setOf(user)
        projectRepository.save(proj)
        projectRepository.save(proj2)
        projectRepository.save(proj3)

        val members = listOf(
            UserOrganizationEntity.of(user, org, "Member"),
            UserOrganizationEntity.of(user2, org, "Admin"),
            UserOrganizationEntity.of(user3, org2, "Owner"),
        )
        userOrganisationRepository.saveAll(members)
//        user.organizations = setOf(org)
    }


    @Test
    fun `user can be manager`() {
        val newuser = UserEntity()
//        newuser.projectsAsManager = mutableSetOf(proj, proj2)

        userRepository.save(newuser)
        val user = userRepository.findById(newuser.id!!).get()
        val dbProj = projectRepository.findById(proj.id!!).get()
//        assertEquals(2, user.projectsAsManager.size)
        assertEquals(1, dbProj.managers.size)
    }

    @Test
    fun `get users in org`() {
        val users = userRepository.getUsersForOrganization(org.id!!)

        val expected = listOf(
            UserInOrg(1, "Janusz", "Member"),
            UserInOrg(2, "Grażyna", "Admin")
        )

        assertEquals(expected, users)
    }
}
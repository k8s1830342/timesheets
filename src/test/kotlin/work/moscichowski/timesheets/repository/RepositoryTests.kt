package work.moscichowski.timesheets.repository

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.data.domain.Pageable
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import work.moscichowski.timesheets.storage.*
import java.time.LocalDate
import java.util.*
import kotlin.collections.HashMap


@ExtendWith(SpringExtension::class)
@DataJpaTest
class RepositoryTests {
    @Autowired
    lateinit var parentRepo: ParentRepository

    @Autowired
    lateinit var childRepo: ChildRepository

    @Test
    fun jpa() {
        val p = Parent("Test")
        val c = Child("Child Test")
//        p.children = mutableSetOf(c)
        c.parent = p
        parentRepo.save(p)
        childRepo.save(c)

        val all = parentRepo.findAllWithChildren()
        println()
    }
}
package work.moscichowski.timesheets.repository

import org.springframework.transaction.annotation.Transactional
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.transaction.annotation.Propagation
import work.moscichowski.timesheets.storage.*
import java.time.LocalDate


@ExtendWith(SpringExtension::class)
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@DataJpaTest
class OrganizationRepositoryTests {
    @Autowired
    lateinit var projectRepository: ProjectRepository

    @Autowired
    lateinit var organizationRepository: OrganizationRepository

    @Autowired
    lateinit var userOrganisationRepository: UserOrganisationRepository

    @Autowired
    lateinit var taskRepository: TaskRepository

    @Autowired
    lateinit var userRepository: UserRepository

    var org = OrganizationEntity("Test Org")
    var org2 = OrganizationEntity("Test Org2")
    var proj = ProjectEntity("Test Proj")
    var proj2 = ProjectEntity("Second Proj")
    var proj3 = ProjectEntity("Third Proj")
    var user = UserEntity("Janusz")
    var user2 = UserEntity("Grażyna")
    var user3 = UserEntity("Hubert")

    @BeforeEach
    fun prepareDB() {
        organizationRepository.save(org)
        organizationRepository.save(org2)
        userRepository.save(user)
        userRepository.save(user2)

        val admin = UserOrganizationEntity.of(user, org, "Admin")
        val member = UserOrganizationEntity.of(user, org2, "Member")
        val noone = UserOrganizationEntity.of(user2, org, "Noone")
        userOrganisationRepository.save(admin)
        userOrganisationRepository.save(member)
        userOrganisationRepository.save(noone)
    }

    @Test
    fun `get organizations for user`()  {
        val organizations = organizationRepository.getOrganizationsForUser2(user.id!!)

        val expected = listOf(
            OrganizationWithRole(1, "Test Org", "Admin"),
            OrganizationWithRole(2, "Test Org2", "Member")
        )

        assertEquals(expected, organizations)
    }

    fun testTask(user: UserEntity): TaskEntity =
        TaskEntity("Test 1", user, proj, LocalDate.of(2000, 2, 8), 1234)

    fun testTask(project: ProjectEntity): TaskEntity =
        TaskEntity("Test 1", user, project, LocalDate.of(2000, 2, 8), 1234)

}
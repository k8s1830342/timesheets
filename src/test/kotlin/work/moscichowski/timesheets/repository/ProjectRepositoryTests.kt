package work.moscichowski.timesheets.repository

import jakarta.transaction.Transactional
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.test.context.junit.jupiter.SpringExtension
import work.moscichowski.timesheets.storage.OrganizationEntity
import work.moscichowski.timesheets.storage.ProjectEntity
import work.moscichowski.timesheets.storage.UserEntity


@ExtendWith(SpringExtension::class)
@DataJpaTest
//@TestExecutionListeners(
//    DependencyInjectionTestExecutionListener::class,
//    TransactionDbUnitTestExecutionListener::class
//)
class ProjectRepositoryTests {
    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var projectRepository: ProjectRepository

    @Autowired
    lateinit var organizationRepository: OrganizationRepository

    @Test
    fun getProjectsInOrganization() {
        val org = OrganizationEntity()
        val org2 = OrganizationEntity()
        val proj = ProjectEntity("Test")
        val proj2 = ProjectEntity("Test2")
        proj.organization = org
        proj2.organization = org2
        org.projects = setOf(proj)
        org2.projects = setOf(proj2)
        organizationRepository.save(org)
        organizationRepository.save(org2)
        projectRepository.save(proj)
        projectRepository.save(proj2)
        val projects: List<ProjectEntity> = projectRepository.getProjectsInOrganization(org.id!!)
        Assertions.assertEquals(1, projects.size)
    }

    @Test
    fun empty() {
        val projects: List<ProjectEntity> = projectRepository.getProjectsInOrganization(1)
        Assertions.assertEquals(0, projects.size)
    }

    @Test
    fun getUsersInProject() {
        val org = OrganizationEntity()
        val proj = ProjectEntity("Test")
        val user1 = UserEntity("Test 1")
        val user2 = UserEntity("Test 2")
        val user3 = UserEntity("Test 3")
        userRepository.save(user1)
        userRepository.save(user2)
        userRepository.save(user3)
        proj.organization = org
        org.projects = setOf(proj)
        organizationRepository.save(org)
//        proj.users = setOf(user1, user2)
        projectRepository.save(proj)

        val project = projectRepository.findById(proj.id!!).get()
        Assertions.assertEquals(2, project.users.size)
        val userFromDb = project.users.first()
//        Assertions.assertEquals("Test 1", userFromDb.name)
    }

    @Test
    fun getProjectsInOrganizationForUser() {
        val org = OrganizationEntity()
        val org2 = OrganizationEntity()
        val proj = ProjectEntity("Test")
        val proj2 = ProjectEntity("Test2")
        val user = UserEntity("Test")
//        user.projects = setOf(proj)
        proj.organization = org
//        proj.users = setOf(user)
        proj2.organization = org2
        org.projects = setOf(proj)
        org2.projects = setOf(proj2)
        userRepository.save(user)
        organizationRepository.save(org)
        organizationRepository.save(org2)
        projectRepository.save(proj)
        projectRepository.save(proj2)
        val projects: Page<ProjectEntity> = projectRepository.getProjects(org.id!!, user.id!!, Pageable.ofSize(100))
        Assertions.assertEquals(1, projects.totalElements)
        Assertions.assertEquals(proj, projects.content[0])
    }

}
package work.moscichowski.timesheets.security

import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import jakarta.servlet.FilterChain
import jakarta.servlet.ServletResponse
import jakarta.servlet.http.HttpServletRequest
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.util.*


class JWTFilterTest {
    @Test
    fun `expired jwt returns 403`() {
        val testKey = Keys.hmacShaKeyFor("testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest".toByteArray())

        val currentDateTime = LocalDateTime.now()
        val oneHourBefore = currentDateTime.minus(1, ChronoUnit.HOURS)
        val date = Date.from(oneHourBefore.atZone(ZoneId.systemDefault()).toInstant())
        val expiredToken = Jwts.builder()
            .subject("42")
            .claim("auth", "authClaims")
            .expiration(
                date
            )
            .signWith(testKey)
            .compact()

        val request = mock(HttpServletRequest::class.java)
        `when`(request.getHeader("Authorization")).thenReturn("Bearer $expiredToken")
        val response = mock(ServletResponse::class.java)
        val chain = mock(FilterChain::class.java)
        val jwtFilter = JWTFilter(testKey)
        assertThrows(ExpiredJwtException::class.java) {
            jwtFilter.doFilter(request, response, chain)
        }
    }
}

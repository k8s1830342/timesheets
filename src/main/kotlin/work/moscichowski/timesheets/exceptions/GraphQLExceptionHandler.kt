package work.moscichowski.timesheets.exceptions

import graphql.ErrorType
import graphql.GraphQLError
import graphql.GraphqlErrorBuilder
import graphql.schema.DataFetchingEnvironment
//import jakarta.validation.ConstraintViolationException
import org.slf4j.LoggerFactory
import org.springframework.graphql.execution.DataFetcherExceptionResolverAdapter
import org.springframework.stereotype.Component
import java.lang.Exception
import org.slf4j.Logger

@Component
class GraphQLExceptionHandler : DataFetcherExceptionResolverAdapter() {
    companion object {
        private val log: Logger? = LoggerFactory.getLogger(this::class.java)
    }

    override fun resolveToSingleError(e: Throwable, env: DataFetchingEnvironment): GraphQLError? {
        return when (e) {
            is ProjectDontExist -> projectNotExists()
//            is ConstraintViolationException -> toValidationException(e)
            is Exception -> toGraphQLError(e)
            else -> super.resolveToSingleError(e, env)
        }
    }

    private fun projectNotExists(): GraphQLError {
        return GraphqlErrorBuilder.newError().message("Project not exists").errorType(ErrorType.DataFetchingException).extensions(
            mapOf("code" to "NOT_FOUND")).build()
    }

    private fun toGraphQLError(e: Throwable): GraphQLError {
        log?.warn("Exception while handling request: ${e.message}", e)
        return GraphqlErrorBuilder.newError().message(e.message).errorType(ErrorType.DataFetchingException).build()
    }
}

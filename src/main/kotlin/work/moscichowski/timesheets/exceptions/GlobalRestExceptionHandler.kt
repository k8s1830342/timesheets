package work.moscichowski.timesheets.exceptions

import io.jsonwebtoken.ExpiredJwtException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

//
//@ControllerAdvice
//class GlobalRestExceptionHandler {
//    private val logger = LoggerFactory.getLogger(this::class.java)
//
//    @ExceptionHandler(ExpiredJwtException::class)
//    fun handleInternalException(e: ExpiredJwtException): ResponseEntity<String> {
//        return ResponseEntity("Token expired", HttpStatus.UNAUTHORIZED)
//    }
//    @ExceptionHandler(LoginError::class)
//    fun handleLogin(e: LoginError): ResponseEntity<String> {
//        return ResponseEntity("Token expired", HttpStatus.UNAUTHORIZED)
//    }
//
//}


@ControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [RuntimeException::class])
    open fun handleConflict(
        ex: RuntimeException?, request: WebRequest?
    ): ResponseEntity<Any> {
        return ResponseEntity("Token expired", HttpStatus.UNAUTHORIZED)
    }
    @ExceptionHandler(value = [LoginError::class])
    open fun s(
        ex: RuntimeException?, request: WebRequest?
    ): ResponseEntity<Any> {
        return ResponseEntity("Token expired", HttpStatus.UNAUTHORIZED)
    }
}

package work.moscichowski.timesheets.exceptions

import java.lang.RuntimeException

class UserAlreadyExists: RuntimeException() {
}
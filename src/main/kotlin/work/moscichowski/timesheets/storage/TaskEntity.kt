package work.moscichowski.timesheets.storage

import jakarta.persistence.*
import java.time.LocalDate

@Entity
@Table(name = "task")
class TaskEntity(
    @Column(nullable = false)
    var title: String,

    @ManyToOne
    @JoinColumn(name = "user_id", nullable=false)
    var user: UserEntity? = null,

    @ManyToOne
    @JoinColumn(name="project_id", nullable=false)
    var project: ProjectEntity? = null,

    @Column(nullable = false)
    var date: LocalDate,

    @Column(nullable = false)
    var duration: Int
) {

    constructor() : this("", null, null, LocalDate.of(1,1,1), 0)


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TaskEntity

        if (title != other.title) return false
        if (user != other.user) return false
        if (project != other.project) return false
        if (date != other.date) return false
        if (duration != other.duration) return false
        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = title.hashCode()
        result = 31 * result + (user?.hashCode() ?: 0)
        result = 31 * result + (project?.hashCode() ?: 0)
        result = 31 * result + date.hashCode()
        result = 31 * result + duration
        result = 31 * result + (id?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "TaskEntity(title='$title', user=$user, project=$project, date=$date, minutes=$duration, id=$id)"
    }

}

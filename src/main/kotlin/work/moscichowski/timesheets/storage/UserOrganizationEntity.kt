package work.moscichowski.timesheets.storage
import jakarta.persistence.*

@Entity
@Table(name = "user_organization")
class UserOrganizationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    var user: UserEntity? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id")
    var organization: OrganizationEntity? = null

    @Column(name = "role")
    var role: String? = null

    companion object {
        fun of(user: UserEntity, organization: OrganizationEntity, role: String): UserOrganizationEntity {
            val userOrganization = UserOrganizationEntity()
            userOrganization.user = user
            userOrganization.organization = organization
            userOrganization.role = role
            return userOrganization
        }
    }
}


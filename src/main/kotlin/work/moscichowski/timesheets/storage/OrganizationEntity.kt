package work.moscichowski.timesheets.storage

import jakarta.persistence.*

@Entity
@Table(name = "organization")
class OrganizationEntity(
    var name: String? = null
) {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int? = null

    @OneToMany(mappedBy = "organization")
    var projects = setOf<ProjectEntity>()

    @OneToMany(mappedBy = "organization", fetch = FetchType.LAZY)
    var users = mutableSetOf<UserOrganizationEntity>()
}
package work.moscichowski.timesheets.storage

import jakarta.persistence.*

@Entity
@Table(name = "project")
class ProjectEntity(
    var name: String? = null
) {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null

    @ManyToOne
    @JoinColumn(name="organization_id", nullable=false)
    var organization: OrganizationEntity? = null

    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY)
    var tasks = setOf<TaskEntity>()

    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY)
    var users = mutableSetOf<UserProjectEntity>()

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "project_manager",
        joinColumns = [JoinColumn(name = "project")],
        inverseJoinColumns = [JoinColumn(name = "user_id")],
    )
    var managers = setOf<UserEntity>()
}
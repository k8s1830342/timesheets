package work.moscichowski.timesheets.storage
import jakarta.persistence.*

@Entity
@Table(name = "user_project")
class UserProjectEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    var user: UserEntity? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id")
    var project: ProjectEntity? = null

    @Column(name = "role")
    var role: String? = null

    companion object {
        fun of(user: UserEntity, project: ProjectEntity, role: String): UserProjectEntity {
            val userProject = UserProjectEntity()
            userProject.user = user
            userProject.project = project
            userProject.role = role
            return userProject
        }
    }
}


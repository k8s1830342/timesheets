package work.moscichowski.timesheets.storage

import jakarta.persistence.*

@Entity
@Table(name="child")
class Child(var name: String? = null) {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null

    @ManyToOne
    @JoinColumn(name="parent_id", nullable=false)
    var parent: Parent? = null
}
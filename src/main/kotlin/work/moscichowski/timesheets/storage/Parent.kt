package work.moscichowski.timesheets.storage

import jakarta.persistence.*

@Entity
@Table(name="parent")
class Parent(var name: String? = null) {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null

    @OneToMany(mappedBy = "parent")
    var children = setOf<Child>()

}
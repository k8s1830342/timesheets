package work.moscichowski.timesheets.storage

import jakarta.persistence.*

@Entity
@Table(name = "organizationInvite")
class OrganizationInviteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null

    @OneToOne
    var organization: OrganizationEntity? = null

    @OneToOne
    var user: UserEntity? = null

    var role: String? = null
}
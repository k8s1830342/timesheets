package work.moscichowski.timesheets.storage

import jakarta.persistence.*

@Entity
class UserEntity(var name: String? = null) {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long? = null

    @OneToMany(mappedBy = "user", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    val organizations = mutableSetOf<UserOrganizationEntity>()

    @OneToMany(mappedBy = "user")
    var tasks = setOf<TaskEntity>()

    @OneToMany(mappedBy = "user", cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    var projects = mutableSetOf<UserProjectEntity>()

    @Column
    var login: String? = null

    @Column
    var password: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserEntity

        if (name != other.name) return false
        if (id != other.id) return false
        if (organizations != other.organizations) return false
        if (tasks != other.tasks) return false
        if (projects != other.projects) return false
        if (login != other.login) return false
        if (password != other.password) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name?.hashCode() ?: 0
        result = 31 * result + (id?.hashCode() ?: 0)
        result = 31 * result + organizations.hashCode()
        result = 31 * result + tasks.hashCode()
        result = 31 * result + projects.hashCode()
        result = 31 * result + (login?.hashCode() ?: 0)
        result = 31 * result + (password?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "UserEntity(name=$name, id=$id, organisations=$organizations, tasks=$tasks, projects=$projects, login=$login, password=$password)"
    }

}

fun UserEntity.of(id: Long, name: String): UserEntity {
    val userEntity = UserEntity(name)
    userEntity.id = id
    return userEntity
}
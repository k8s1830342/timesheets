package work.moscichowski.timesheets.service

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import work.moscichowski.timesheets.repository.UserRepository
import work.moscichowski.timesheets.security.AuthUser

@Service
class UserService(
    val userRepository: UserRepository
    ) {
    fun getCurrentUser(): User {
        val userId = (SecurityContextHolder.getContext().authentication.principal as? AuthUser)?.id
        val user = userRepository.findById(userId!!).get()
        return User(userId, user.name!!)
    }
}
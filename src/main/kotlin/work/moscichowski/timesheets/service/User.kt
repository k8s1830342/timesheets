package work.moscichowski.timesheets.service

data class User(val id: Long, val name: String)

package work.moscichowski.timesheets.service

import io.jsonwebtoken.Jwts
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import work.moscichowski.timesheets.exceptions.LoginError
import work.moscichowski.timesheets.exceptions.UserAlreadyExists
import work.moscichowski.timesheets.repository.UserRepository
import work.moscichowski.timesheets.storage.UserEntity
import java.util.*
import javax.crypto.SecretKey

@Service
class LoginService(
    val userRepository: UserRepository,
    val bCryptPasswordEncoder: BCryptPasswordEncoder,
    val key: SecretKey
) {

    fun login(login: String, password: String): String {
        val user = userRepository.findByLogin(login)
        if (!bCryptPasswordEncoder.matches(password, user?.password)) {
            throw LoginError()
        }
        return Jwts.builder()
            .subject(user!!.id.toString())
            .claim("auth", "authClaims")
            .expiration(
                Date().add(Calendar.HOUR, 1)
            )
            .signWith(key)
            .compact()
    }

    fun register(login: String, password: String, username: String): String {
        if (userRepository.findByLogin(login) != null) {
            throw UserAlreadyExists()
        }
        val user = UserEntity()
        user.login = login
        user.password = bCryptPasswordEncoder.encode(password)
        user.name = username
        val saved = userRepository.save(user)
        return Jwts.builder()
            .subject(saved.id.toString())
            .claim("auth", "authClaims")
            .expiration(
                Date().add(Calendar.HOUR, 1)
            )
            .signWith(key)
            .compact()
    }
}

// kudos to https://gist.github.com/maiconhellmann/796debb4007139d50e39f139be08811c
fun Date.add(field: Int, amount: Int): Date {
    Calendar.getInstance().apply {
        time = this@add
        add(field, amount)
        return time
    }
}

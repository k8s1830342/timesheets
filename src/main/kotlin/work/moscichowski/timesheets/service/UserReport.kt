package work.moscichowski.timesheets.service

data class UserReport(val minutes: Long)
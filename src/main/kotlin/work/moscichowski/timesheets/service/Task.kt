package work.moscichowski.timesheets.service

import java.time.LocalDate

data class Task(val id: Long, val title: String, val date: LocalDate, val minutes: Int, val user: User, val project: Project)

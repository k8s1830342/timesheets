package work.moscichowski.timesheets.service

import org.springframework.stereotype.Service
import org.springframework.security.core.context.SecurityContextHolder
import work.moscichowski.timesheets.repository.TaskRepository
import work.moscichowski.timesheets.security.AuthUser
import java.time.LocalDate

@Service
class ReportsService(val taskRepository: TaskRepository) {
    fun getUserReport(from: LocalDate, to: LocalDate): UserReport {
        val userId = (SecurityContextHolder.getContext().authentication.principal as? AuthUser)?.id

        return UserReport(taskRepository.getUserReport(userId!!, from, to))
    }
}
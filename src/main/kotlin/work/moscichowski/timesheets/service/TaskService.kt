package work.moscichowski.timesheets.service

import jakarta.transaction.Transactional
import org.springframework.data.domain.Pageable
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import work.moscichowski.timesheets.exceptions.NoManagerPermissions
import work.moscichowski.timesheets.exceptions.ProjectDontExist
import work.moscichowski.timesheets.exceptions.UserNotInProject
import work.moscichowski.timesheets.repository.ProjectRepository
import work.moscichowski.timesheets.repository.TaskRepository
import work.moscichowski.timesheets.repository.UserRepository
import work.moscichowski.timesheets.security.AuthUser
import work.moscichowski.timesheets.storage.TaskEntity
import java.time.LocalDate
import kotlin.jvm.optionals.getOrElse


@Service
class TaskService(
    val taskRepository: TaskRepository,
    val projectRepository: ProjectRepository,
    val userRepository: UserRepository
) {

    @Transactional
    fun addTask(title: String, projectId: Long, date: LocalDate, minutes: Int): Task {
        val project = projectRepository.findById(projectId).getOrElse { throw ProjectDontExist() }
        val userId = (SecurityContextHolder.getContext().authentication.principal as? AuthUser)?.id
        if (userId !in project.users.map { it.id }) {
            throw UserNotInProject()
        }

        val user = userRepository.findById(userId!!).get()
        val task = TaskEntity(title, user, project, date, minutes)
        try {
            val saved = taskRepository.save(task)
            return Task(
                saved.id!!,
                saved.title,
                saved.date,
                saved.duration,
                User(saved.user!!.id!!, saved.user!!.name!!),
                Project(
                    saved.project!!.id!!, saved.project!!.name!!
                )
            )
        } catch (e: Exception) {
            println(e)
            return Task(1, "", LocalDate.of(1, 1, 1), 1, User(1, "XD"), Project(1, "XD2"))
        }

    }

    @Transactional
    fun addTask(userId: Long, title: String, projectId: Long, date: LocalDate, minutes: Int): Task {
        val project = projectRepository.findById(projectId).getOrElse { throw ProjectDontExist() }
        val managerId = (SecurityContextHolder.getContext().authentication.principal as? AuthUser)?.id
        val managerUser = userRepository.findById(managerId!!).get()

//        if (!managerUser.projectsAsManager.map { it.id }.contains(projectId)) {
//            throw NoManagerPermissions()
//        }

        if (!project.users.map { it.id }.contains(userId)) {
            throw UserNotInProject()
        }

        val user = userRepository.findById(userId).get()
        val task = TaskEntity(title, user, project, date, minutes)
        val saved = taskRepository.save(task)
        return Task(
            saved.id!!, saved.title, saved.date, saved.duration, User(saved.user!!.id!!, saved.user!!.name!!), Project(
                saved.project!!.id!!, saved.project!!.name!!
            )
        )
    }

    fun updateTask(id: Long, title: String?, projectId: Long?, date: LocalDate?, duration: Int?) {
        val authUser = SecurityContextHolder.getContext().authentication.principal as? AuthUser
        val userId = authUser?.id
        val savedTask = taskRepository.findById(id).get()
        if (savedTask.user?.id != userId) {
            val manager = userRepository.findById(authUser!!.id).get()
//            if (!manager.projectsAsManager.map { it.id }.contains(savedTask.project?.id)) {
//                throw NoManagerPermissions()
//            }
        }
        savedTask.title = title ?: savedTask.title
        savedTask.date = date ?: savedTask.date
        savedTask.duration = duration ?: savedTask.duration
        if (projectId != null) {
            val proj = projectRepository.findById(projectId).get()
            if (!proj.users.map { it.id }.contains(userId)) {
                throw UserNotInProject()
            }
            savedTask.project = proj
        }

        taskRepository.save(savedTask)
    }

    fun getTasks(
        organizationId: Int? = null,
        projectIds: Set<Long>? = null,
        userIds: Set<Long>? = null,
        from: LocalDate,
        to: LocalDate
    ): PaginatedTasks {
        val authUser = SecurityContextHolder.getContext().authentication.principal as? AuthUser
        val currentUser = userRepository.findById(authUser!!.id).get()
//        val projectsWithPermissions = if (projectIds != null) {
//            currentUser.projectsAsManager.mapNotNull { it.id }.intersect(projectIds)
//        } else {
//            currentUser.projectsAsManager.mapNotNull { it.id }
//        }
//        if (projectsWithPermissions.isEmpty()) {
//            throw NoManagerPermissions()
//        }
        val uIds = userIds ?: setOf()
//        val res = taskRepository.getTasks(organizationId, projectsWithPermissions.toSet(), uIds, from, to, Pageable.ofSize(100))
        val res = taskRepository.getTasks(organizationId, setOf(), uIds, from, to, Pageable.ofSize(100))

        val mapped = res.content.map {
            Task(
                it.id!!,
                it.title,
                it.date,
                it.duration,
                User(it.user!!.id!!, it.user!!.name!!),
                Project(it.project!!.id!!, it.project!!.name!!)
            )
        }
        return PaginatedTasks(res.totalElements, mapped, 0)
    }


    fun getMyTasks(organizationId: Int? = null, from: LocalDate, to: LocalDate): PaginatedTasks {
        val authUser = SecurityContextHolder.getContext().authentication.principal as? AuthUser
        val res = taskRepository.getTasks(organizationId, setOf(), setOf(authUser!!.id), from, to, Pageable.ofSize(100))

        val mapped = res.content.map {
            Task(
                it.id!!,
                it.title,
                it.date,
                it.duration,
                User(it.user!!.id!!, it.user!!.name!!),
                Project(it.project!!.id!!, it.project!!.name!!)
            )
        }
        return PaginatedTasks(res.totalElements, mapped, 0)
    }
}
package work.moscichowski.timesheets.service

import org.springframework.data.domain.Page
import work.moscichowski.timesheets.repository.ProjectWithRole

data class PaginatedProjects(val total: Long, val projects: Page<ProjectWithRole>, val pageNo: Int)
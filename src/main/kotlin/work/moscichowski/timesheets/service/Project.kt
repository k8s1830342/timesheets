package work.moscichowski.timesheets.service

data class Project(val id: Long, val name: String)

package work.moscichowski.timesheets.service

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import work.moscichowski.timesheets.repository.*
import work.moscichowski.timesheets.security.AuthUser
import work.moscichowski.timesheets.storage.OrganizationEntity
import work.moscichowski.timesheets.storage.OrganizationInviteEntity
import work.moscichowski.timesheets.storage.UserOrganizationEntity

@Service
class OrganizationService(
    val userRepository: UserRepository,
    val organizationRepository: OrganizationRepository,
    val organisationInviteRepository: OrganisationInviteRepository,
    val userOrganisationRepository: UserOrganisationRepository
    ) {
    fun addOrganization(name: String): Organization {
        val userId = (SecurityContextHolder.getContext().authentication.principal as? AuthUser)?.id
        val user = userRepository.findById(userId!!).get()

        val organization = OrganizationEntity()
        organization.name = name
        val saved = organizationRepository.save(organization)
        val assignment = UserOrganizationEntity.of(user, saved, "Owner")
        userOrganisationRepository.save(assignment)
        return Organization(saved.id!!, name)
    }

    fun getUserOrganizations(): List<OrganizationWithRole> {
        val userId = (SecurityContextHolder.getContext().authentication.principal as? AuthUser)?.id
//        val user = userRepository.findById(userId!!).get()

        return organizationRepository.getOrganizationsForUser2(userId!!)
    }

    fun inviteUser(userId: Long, orgId: Int, role: String): Invitation {
        val user = userRepository.findById(userId).get()
        val organization = organizationRepository.findById(orgId).get()

        val invite = OrganizationInviteEntity()
        invite.role = role
        invite.user = user
        invite.organization = organization

        val id = organisationInviteRepository.save(invite).id
        return Invitation(id!!, organization.id!!, role)
    }

    fun acceptInvitation(invitationId: Long): Boolean {
        val userId = (SecurityContextHolder.getContext().authentication.principal as? AuthUser)?.id
        val invite = organisationInviteRepository.findById(invitationId).get()

        if (invite.user?.id != userId) {
            throw RuntimeException()
        }

        val assignment = UserOrganizationEntity.of(invite.user!!, invite.organization!!, invite.role!!)
        userOrganisationRepository.save(assignment)

        return true
    }
}

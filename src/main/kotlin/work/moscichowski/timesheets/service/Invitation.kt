package work.moscichowski.timesheets.service

data class Invitation(val id: Long, val orgId: Int, val role: String)

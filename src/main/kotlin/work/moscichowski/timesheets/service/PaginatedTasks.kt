package work.moscichowski.timesheets.service

data class PaginatedTasks(val total: Long, val tasks: List<Task>, val pageNo: Int)
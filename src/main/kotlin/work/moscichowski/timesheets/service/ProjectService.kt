package work.moscichowski.timesheets.service

import org.springframework.data.domain.Pageable
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import work.moscichowski.timesheets.repository.*
import work.moscichowski.timesheets.security.AuthUser
import work.moscichowski.timesheets.storage.ProjectEntity
import work.moscichowski.timesheets.storage.UserProjectEntity

@Service
class ProjectService(
    val projectRepository: ProjectRepository,
    val organizationRepository: OrganizationRepository,
    val userRepository: UserRepository,
    val userProjectRepository: UserProjectRepository
) {
    fun addProject(organizationId: Int, name: String): Project {
        val userId = (SecurityContextHolder.getContext().authentication.principal as? AuthUser)?.id
        val user = userRepository.findById(userId!!).get()
        val org = organizationRepository.findById(organizationId).get()
        val project = ProjectEntity(name)
        project.managers = setOf(user)
        project.organization = org
        val let = projectRepository.save(project).let { Project(it.id!!, it.name!!) }
        userProjectRepository.save(UserProjectEntity.of(user, project, "OWNER"))
        return let
    }

    fun addUser(projectId: Long, userId: Long, role: String): Boolean {
        val user = userRepository.findById(userId).get()
        val project = projectRepository.findById(projectId).get()
        val assignment = UserProjectEntity.of(user, project, role)
        userProjectRepository.save(assignment)
        return true
    }

    fun getProjects(organizationId: Int): PaginatedProjects {
        val userId = (SecurityContextHolder.getContext().authentication.principal as? AuthUser)?.id
        val user = userRepository.findById(userId!!).get()
        val org = organizationRepository.findById(organizationId).get()
        val res = projectRepository.getProjects2(org.id!!, user.id!!, Pageable.ofSize(100))
        val map = res.content.map {
            it
        }
        return PaginatedProjects(res.totalElements, res, 1)
    }

}

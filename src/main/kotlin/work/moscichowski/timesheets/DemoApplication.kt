package work.moscichowski.timesheets

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import work.moscichowski.timesheets.repository.*
import work.moscichowski.timesheets.storage.*
import java.time.LocalDate

@SpringBootApplication
class DemoApplication {
//	    @Bean
//		fun demo(
//			organizationRepository: OrganizationRepository,
//			userRepository: UserRepository,
//			userOrganisationRepository: UserOrganisationRepository
//			): CommandLineRunner {
//			return CommandLineRunner {
//				var org = OrganizationEntity("Test Org")
//				var org2 = OrganizationEntity("Test Org2")
//				var user = UserEntity("Janusz")
//				var user2 = UserEntity("Grażyna")
//
//				organizationRepository.save(org)
//				organizationRepository.save(org2)
//				userRepository.save(user)
//				userRepository.save(user2)
//
//				val admin = UserOrganizationEntity.of(user, org, "Admin")
//				val member = UserOrganizationEntity.of(user, org, "Member")
//				userOrganisationRepository.save(admin)
//				userOrganisationRepository.save(member)
//
////        val xzldkc = userRepository.getAll()
////        val xzxz = userRepository.findAll().mapNotNull { it }
//				val x = organizationRepository.findAllWithUsers().map { it }
//				println()
//			}
//		}

}

fun main(args: Array<String>) {
	runApplication<DemoApplication>(*args)
}

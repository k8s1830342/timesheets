package work.moscichowski.timesheets.security

import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import work.moscichowski.timesheets.storage.UserEntity

class JWTAuth(private var user: UserEntity): Authentication {


    override fun getName(): String {
        return user.name ?: ""
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return mutableSetOf(GrantedAuthority { "ROLE_USER" })
    }

    override fun getCredentials(): Any {
        return "Test1"
    }

    override fun getDetails(): Any {
        return "Test2"
    }

    override fun getPrincipal(): Any {
        return AuthUser(user.id!!)
    }

    override fun isAuthenticated(): Boolean {
        return true
    }

    override fun setAuthenticated(isAuthenticated: Boolean) {
    }
}
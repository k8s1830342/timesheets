package work.moscichowski.timesheets.security

import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.security.Keys
import jakarta.servlet.*
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import jakarta.transaction.Transactional
import org.springframework.http.HttpStatus
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import work.moscichowski.timesheets.exceptions.LoginError
import work.moscichowski.timesheets.repository.UserRepository
import work.moscichowski.timesheets.storage.UserEntity
import javax.crypto.SecretKey

@Component
class JWTFilter(
  private val key: SecretKey
): Filter {

  override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
    val token = (request as HttpServletRequest).getHeader("Authorization")
    if (token == null) {
      chain.doFilter(request, response)
      return
    }
    try {
      val parseSignedClaims = Jwts.parser()
        .verifyWith(key)
        .clockSkewSeconds(3 * 60)
        .build()
        .parseSignedClaims(token.replace("Bearer ", ""))
      val user1 = UserEntity()
      user1.id = parseSignedClaims.payload.subject.toLong()
      SecurityContextHolder.getContext().authentication = JWTAuth(user1)
      chain.doFilter(request, response)

    } catch (e: ExpiredJwtException) {
        (response as HttpServletResponse).status = HttpStatus.UNAUTHORIZED.value()
        response.writer.write("{}")
    } catch (e: MalformedJwtException) {
        (response as HttpServletResponse).status = HttpStatus.UNAUTHORIZED.value()
        response.writer.write("{}")
    }
  }
}

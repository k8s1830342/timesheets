package work.moscichowski.timesheets.controller

import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.stereotype.Controller
import work.moscichowski.timesheets.service.PaginatedProjects
import work.moscichowski.timesheets.service.Project
import work.moscichowski.timesheets.service.ProjectService

@Controller
class ProjectController(val projectService: ProjectService) {

    @MutationMapping
    fun addProject(@Argument organizationId: Int, @Argument name: String): Project {
        return projectService.addProject(organizationId, name)
    }

    @MutationMapping
    fun addUserToProject(@Argument projectId: Long, @Argument userId: Long, @Argument role: String): Boolean {
        return projectService.addUser(projectId, userId, role)
    }

    @QueryMapping
    fun getProjects(@Argument organizationId: Int): PaginatedProjects {
        return projectService.getProjects(organizationId)
    }

}
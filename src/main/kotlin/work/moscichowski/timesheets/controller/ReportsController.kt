package work.moscichowski.timesheets.controller

import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.stereotype.Controller
import work.moscichowski.timesheets.service.PaginatedTasks
import work.moscichowski.timesheets.service.ReportsService
import work.moscichowski.timesheets.service.UserReport
import java.time.LocalDate

@Controller
class ReportsController(val reportsService: ReportsService) {

    @QueryMapping
    fun getUserReport(@Argument("from") from: LocalDate, @Argument("to") to: LocalDate): UserReport {
        return reportsService.getUserReport(from, to)
    }


}
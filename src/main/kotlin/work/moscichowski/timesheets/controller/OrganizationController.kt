package work.moscichowski.timesheets.controller

import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.BatchMapping
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.graphql.data.method.annotation.SchemaMapping
import org.springframework.stereotype.Controller
import work.moscichowski.timesheets.repository.OrganizationWithRole
import work.moscichowski.timesheets.service.Invitation
import work.moscichowski.timesheets.service.Organization
import work.moscichowski.timesheets.service.OrganizationService
import work.moscichowski.timesheets.storage.Child
import work.moscichowski.timesheets.storage.Parent

@Controller
class OrganizationController(val organizationService: OrganizationService) {

    @QueryMapping
    fun getOrganizations(): List<OrganizationWithRole> {
        return organizationService.getUserOrganizations()
    }

    @MutationMapping
    fun addOrganization(@Argument name: String): Organization {
        return organizationService.addOrganization(name)
    }

    @MutationMapping
    fun inviteUser(@Argument userId: Long, @Argument organizationId: Int, @Argument role: String): Long {
        return organizationService.inviteUser(userId, organizationId, role).id
    }

    @MutationMapping
    fun acceptInvitation(@Argument invitationId: Long): Boolean {
        return organizationService.acceptInvitation(invitationId)
    }

    @QueryMapping
    fun parents(): List<Parent> {
        val child = Child()
        child.id = 654

        val element = Parent("test")
        val element2 = Parent("test2")

        element.id = 123456
        element.children = setOf(child)
        element2.id = 12

        return listOf(element, element2)
    }

    @BatchMapping
    fun children(parents: List<Parent>): Map<Parent, List<Child>> {
        return parents.associateBy({ it }, {
            val c = Child()
            c.id = 564
            return@associateBy listOf(c)
        })
//        return parents.map {
//            val c = Child()
//            c.id = 564
//            return@map it to listOf(c)
//        }.toMap()
    }
}
package work.moscichowski.timesheets.controller

data class LoginRequest(val login: String, val password: String)
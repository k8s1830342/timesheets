package work.moscichowski.timesheets.controller

import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import work.moscichowski.timesheets.service.*
import java.time.LocalDate
import java.util.stream.IntStream


data class Category(val id: Int, val title: String)
data class Refine(val id: Int, val title: String, val category: Category)

@Controller
class TasksController(val service: TaskService) {

    @QueryMapping
    fun tasks(@Argument organizationId: Int?, @Argument("userId") userId: Long?, @Argument("projectId") projectId: Long?, @Argument("from") from: LocalDate, @Argument("to") to: LocalDate): PaginatedTasks {
        return if (userId == null && projectId == null){
            service.getMyTasks(organizationId, from, to)
        } else {
            service.getTasks(organizationId, setOf(userId!!), setOf(projectId!!), from, to)
        }
    }

    @QueryMapping
    @PreAuthorize("hasRole('ADMIN')")
    fun admin(): Int {
        return 24
    }

    @MutationMapping
    fun addTask(@Argument userId: Long?, @Argument title: String, @Argument projectId: Long, @Argument date: LocalDate, @Argument duration: Int): Task {
        return if (userId == null) {
            service.addTask(title, projectId, date, duration)
        } else {
            service.addTask(userId, title, projectId, date, duration)
        }
    }

    @MutationMapping
    fun updateTask(@Argument("id") id: Long, @Argument("title") title: String?, @Argument("projectId") projectId: Long?, @Argument("date") date: LocalDate?, @Argument("duration") duration: Int?): Task {
        println("task id $id")
        return Task(4567 , "todo", LocalDate.of(1, 1, 1), 123, User(1, "todo"), Project(1, "todo"))
    }
}


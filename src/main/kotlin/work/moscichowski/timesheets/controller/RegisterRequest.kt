package work.moscichowski.timesheets.controller

data class RegisterRequest(val login: String, val password: String, val username: String)
package work.moscichowski.timesheets.controller

import org.springframework.graphql.data.method.annotation.Argument
import org.springframework.graphql.data.method.annotation.BatchMapping
import org.springframework.graphql.data.method.annotation.MutationMapping
import org.springframework.graphql.data.method.annotation.QueryMapping
import org.springframework.graphql.data.method.annotation.SchemaMapping
import org.springframework.stereotype.Controller
import work.moscichowski.timesheets.repository.OrganizationWithRole
import work.moscichowski.timesheets.service.*
import work.moscichowski.timesheets.storage.Child
import work.moscichowski.timesheets.storage.Parent

@Controller
class UserController(val userService: UserService) {

    @QueryMapping
    fun getCurrentUser(): User {
        return userService.getCurrentUser()
    }

}
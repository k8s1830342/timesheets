package work.moscichowski.timesheets.controller

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import work.moscichowski.timesheets.exceptions.LoginError
import work.moscichowski.timesheets.service.LoginService

@RestController
class LoginController(val loginService: LoginService) {
    @PostMapping(path = ["/login"])
    fun login(@RequestBody req: LoginRequest): String {
        return loginService.login(req.login, req.password)
    }

    @PostMapping(path = ["/register"])
    fun register(@RequestBody req: RegisterRequest): String {
        return loginService.register(req.login, req.password, req.username)
    }
}
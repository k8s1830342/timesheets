package work.moscichowski.timesheets.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import work.moscichowski.timesheets.storage.ProjectEntity


interface ProjectRepository: CrudRepository<ProjectEntity, Long> {
    @Query(
        value = "select id, organization_id, name from project where organization_id =?1",
        nativeQuery = true
    )
    fun getProjectsInOrganization(organizationId: Int): List<ProjectEntity>

    @Query(
        value = "select id, organization_id, name from project join project_user on (project.id = project_user.project) where organization_id =?1 AND user_id = ?2",
        countQuery = "select count(*) from project join project_user on (project.id = project_user.project) where organization_id =?1 AND user_id = ?2",
        nativeQuery = true
    )
    fun getProjects(organizationId: Int, userId: Long, pageable: Pageable): Page<ProjectEntity>

    @Query(
        value = "SELECT new work.moscichowski.timesheets.repository.ProjectWithRole(p.id, p.name, u.role) FROM ProjectEntity p LEFT JOIN p.users u WHERE p.organization.id =?1 AND u.user.id =?2",
        countQuery = "SELECT COUNT(*) FROM ProjectEntity p LEFT JOIN p.users u WHERE p.organization.id =?1 AND u.user.id =?2"
    )
    fun getProjects2(organizationId: Int, userId: Long, pageable: Pageable): Page<ProjectWithRole>
}

data class ProjectWithRole(
    var id: Long,
    val name: String,
    val role: String
)

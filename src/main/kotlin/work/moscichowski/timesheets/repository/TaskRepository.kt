package work.moscichowski.timesheets.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import work.moscichowski.timesheets.storage.TaskEntity
import java.time.LocalDate

interface TaskRepository: CrudRepository<TaskEntity, Long> {

    @Query(
        value = "select id, title, user_id, project_id, date, duration from task where user_id =?1",
        nativeQuery = true
    )
    fun getTaskforUser(userId: Long): List<TaskEntity>

    @Query(
        value = "select id, title, user_id, project_id, date, duration from task where project_id =?1",
        nativeQuery = true
    )
    fun getTaskInProject(projectId: Long): List<TaskEntity>

//    @Query(
//        value = "select id, title, user_id, project_id, date, duration from task where project_id =?1 AND user_id =?2 AND date >= ?3 AND date <= ?4",
//        nativeQuery = true
//    )
//    fun getTasks(projectId: Long, userId: Long, startDate: LocalDate, endDate: LocalDate): List<TaskEntity>

    @Query(
        value = "select id, title, user_id, project_id, date, duration from task where project_id =?1 AND date >= ?2 AND date <= ?3",
        nativeQuery = true
    )
    fun getTaskInProjectForPeriod(projectId: Long, startDate: LocalDate, endDate: LocalDate): List<TaskEntity>

    @Query(
        value = "select id, title, user_id, project_id, date, duration from task where project_id =?1 AND date >= ?2 AND date <= ?3",
        nativeQuery = true
    )
    fun getTaskInProjectForPeriodForUser(
        projectId: Long,
        startDate: LocalDate,
        endDate: LocalDate,
        userId: Long
    ): List<TaskEntity>

    @Query(
        value = "select task.id, title, user_id, project_id, date, duration from task join project on (task.project_id = project.id) where (?1 IS NULL OR organization_id = ?1) AND (?2 IS NULL or (project_id in ?2)) AND (?3 IS NULL or (user_id in ?3)) AND date >= ?4 AND date <= ?5",
        countQuery = "SELECT count(*) from task where (?1 is null OR project_id in ?1) AND (?2 is null OR user_id in ?2) AND date >= ?3 AND date <= ?4",
        nativeQuery = true
    )
    fun getTasks(organizationId: Int?, projectsId: Set<Long>, usersId: Set<Long>, startDate: LocalDate, endDate: LocalDate, pageable: Pageable): Page<TaskEntity>

    @Query(
        value = "SELECT SUM(duration) FROM TaskEntity WHERE user.id =?1 AND date >= ?2 AND date <= ?3 ",
    )
    fun getUserReport(userId: Long, from: LocalDate, to: LocalDate): Long
}
package work.moscichowski.timesheets.repository

import org.springframework.data.repository.CrudRepository
import work.moscichowski.timesheets.storage.UserOrganizationEntity

interface UserOrganisationRepository : CrudRepository<UserOrganizationEntity, Long>

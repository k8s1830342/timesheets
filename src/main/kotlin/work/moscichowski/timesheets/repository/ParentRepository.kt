package work.moscichowski.timesheets.repository

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import work.moscichowski.timesheets.storage.Parent

interface ParentRepository: CrudRepository<Parent, Long> {
    @Query("SELECT p FROM Parent p JOIN FETCH p.children")
    fun findAllWithChildren(): List<Parent>
}
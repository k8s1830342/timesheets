package work.moscichowski.timesheets.repository

import org.springframework.data.repository.CrudRepository
import work.moscichowski.timesheets.storage.OrganizationInviteEntity
import work.moscichowski.timesheets.storage.UserOrganizationEntity

interface OrganisationInviteRepository : CrudRepository<OrganizationInviteEntity, Long>

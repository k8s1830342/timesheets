package work.moscichowski.timesheets.repository

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import work.moscichowski.timesheets.storage.OrganizationEntity

interface OrganizationRepository: CrudRepository<OrganizationEntity, Int> {

    @Query("SELECT o FROM OrganizationEntity o LEFT JOIN FETCH o.users u LEFT JOIN FETCH u.user")
    fun findAllWithUsers(): List<OrganizationEntity>
    @Query(
        value = "select id, name from organization join organization_user on (organization_user.organization = organization.id) where user_id = ?1",
        nativeQuery = true)
    fun getOrganizationsForUser(id: Long): List<OrganizationEntity>

//    @Query(
//        value = "select organization.id, organization.name, role from organization join user_organization on (user_organization.organization_id = organization.id) where user_id = ?1",
//        nativeQuery = true)
//    fun getOrganizationsForUser(id: Long): List<OrganizationEntity>

    @Query("SELECT new work.moscichowski.timesheets.repository.OrganizationWithRole(o.id, o.name, u.role) FROM OrganizationEntity o LEFT JOIN o.users u WHERE u.user.id =?1")
//    @Query(
//    value = "select organization.id as id, name as name, user_organization.role from organization join user_organization on (user_organization.organization_id = organization.id) where user_id = ?1",
//    nativeQuery = true)
    fun getOrganizationsForUser2(id: Long): List<OrganizationWithRole>
}

data class OrganizationWithRole(
    var id: Long,
    val name: String,
    val role: String
)

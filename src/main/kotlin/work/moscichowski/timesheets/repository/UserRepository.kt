package work.moscichowski.timesheets.repository

import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import work.moscichowski.timesheets.storage.OrganizationEntity
import work.moscichowski.timesheets.storage.UserEntity

interface UserRepository: CrudRepository<UserEntity, Long> {
    fun findByLogin(name: String?): UserEntity?

    @Query(
        value = "select user_entity.id, login, password, name, user_organization.role, from user_entity join user_organization on (user_entity.id = user_organization.user_id)",
        nativeQuery = true
    )
    fun getAll(): List<UserEntity>

    @Query("SELECT u FROM UserEntity u LEFT JOIN FETCH u.organizations o LEFT JOIN FETCH o.organization")
    fun getOrganizationsForUser(): List<UserEntity>

//    @Query("SELECT new work.moscichowski.timesheets.repository.OrganizationWithRole(o.id, o.name, u.role) FROM OrganizationEntity o LEFT JOIN o.users u WHERE u.user.userid =?1")
    @Query("SELECT new work.moscichowski.timesheets.repository.UserInOrg(u.id, u.name, o.role) FROM UserEntity u LEFT JOIN u.organizations o WHERE o.organization.id =?1")
    fun getUsersForOrganization(id: Int): List<UserInOrg>
}

data class UserInOrg(
    val id: Long,
    val name: String,
    val role: String
)
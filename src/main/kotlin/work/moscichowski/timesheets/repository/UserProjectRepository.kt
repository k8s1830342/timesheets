package work.moscichowski.timesheets.repository

import org.springframework.data.repository.CrudRepository
import work.moscichowski.timesheets.storage.UserProjectEntity

interface UserProjectRepository : CrudRepository<UserProjectEntity, Long>

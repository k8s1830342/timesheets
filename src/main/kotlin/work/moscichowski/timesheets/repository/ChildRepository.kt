package work.moscichowski.timesheets.repository

import org.springframework.data.repository.CrudRepository
import work.moscichowski.timesheets.storage.Child

interface ChildRepository: CrudRepository<Child, Long> {
}
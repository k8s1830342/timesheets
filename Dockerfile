FROM openjdk:17-alpine
COPY build/libs/demo-0.0.1-SNAPSHOT.jar ./app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "app.jar"]
EXPOSE 8080
